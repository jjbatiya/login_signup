import React from 'react';
import {
    View,
    Text,
    alert,
    StyleSheet, Dimensions,Image,TouchableOpacity, LogBox
}from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';


const SplashScreen=({navigation})=>
{
    const[data,setData]=React.useState({
        user_id:'',
     });
 
    const checkLogin=()=>{

        AsyncStorage.getItem("user_id").then((value) => {
          
        if(value=='21')
            navigation.replace("Admin");
        else if(value!='0'&&value!=null)
            navigation.replace("Home");
        else
            navigation.replace("SignIn")
        });
        
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Image source={require('../asset/logo.png')}
                    style={styles.logo}
                    resizeMode='stretch'>

                </Image>
            </View>
            <View style={styles.footer}>
                <Text style={styles.title}>Stay connected with everyone!</Text>
                <Text style={styles.text}>Sign in with account</Text>
                <View style={styles.buttom}>

                <TouchableOpacity onPress={checkLogin}>
                    <LinearGradient
                             colors={['#08d4c4','#01ab9d']}
                             style={styles.signIn}>
                            <Text style={styles.textSign}>Get Started</Text>

                    </LinearGradient>
                    </TouchableOpacity>
                </View>
              
            </View>
        </View>
    );
}
export default SplashScreen;

const {height}=Dimensions.get("screen");
const height_logo =height*0.28;

const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#009387'
    },
    header:{
        flex:2,
        justifyContent:"center",
        alignItems:"center"
    },
    footer:{
        flex:1,
        backgroundColor:"#fff",
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        paddingVertical:50,
        paddingHorizontal:30
    },
    logo:{
        width:height_logo,
        height:height_logo
    },
    title:{
        color:'#05375a',
        fontSize:30,
        fontWeight:'bold'
    },
    text:{
        color:'grey',
        marginTop:5
    },
    buttom:{
        alignItems:'flex-end',
        marginTop:30
    },
    signIn:{
        width:150,
        height:40,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:50,
        flexDirection:'row'
    },
    textSign:{
        color:'white',
        fontWeight:"bold"
    },
    btn:{
        padding:15,
        marginTop:20,
        borderRadius:30,
        marginLeft:150,
        alignItems:'center',
       backgroundColor:'#009387'
    }
});