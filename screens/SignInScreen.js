import React from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';


import {
    View,
    Text,
    Button,
    StyleSheet,Dimensions,TextInput,TouchableOpacity,ToastAndroid,Alert
}from 'react-native';
import AsyncStorage from  '@react-native-community/async-storage';
let reg = /^[0-9]{10}$/;

const SignInScreen=({navigation})=>
{
    
    const[data,setData]=React.useState({
       monile_no:'',
       password:'',
       check_textInputChange:false,
       secureTextEntry:true 
    });
    
    const textInputChange=(val)=>
    {
        if(reg.test(val)!=false)
        {
            setData({
                ...data,
                monile_no:val,
                 check_textInputChange:true
            })
        }
        else{
            setData({
                ...data,
                monile_no:val,
                 check_textInputChange:false
            })
        }
    }
    const handlePasswordchange=(val)=>
    {
        setData({
            ...data,
            password:val
        })
    }
    const updateSecureTextEntry=()=>{
        setData({
            ...data,
            secureTextEntry:!data.secureTextEntry
        })
    }
    const validate_func=()=>
    {
        
        if(!data.check_textInputChange)
        {
            ToastAndroid.show("Mobile No. is Not Valid!",ToastAndroid.SHORT);
            return;
        }
        else if(data.password.length==0)
        {
            ToastAndroid.show("Password is Empty!",ToastAndroid.SHORT);
            return;
        }
        else
        {
            fetch('https://jitubatiya.000webhostapp.com/react_demo/api.php', {
                method: 'post',
                header:{
                  'Accept':'application/json',
                  'Content-type':'application/json'
                },
                body:JSON.stringify({
                    type:"SignIn",
                    name:'',
                    mobile_no:data.monile_no,
                    u_password:data.password
                })
             })
             .then((response) => response.json())
             .then((responseJson) => {
                //var obj = JSON.parse(response);
                console.log(responseJson['msg']);
                if(responseJson['status'])
                {
                    let text1="Home";
                    if(responseJson['role']=="1")
                        text1="Admin";
                    AsyncStorage.setItem('user_id',responseJson['id']);
                    Alert.alert("Message",responseJson['msg']+"",[{text:"OK",onPress:()=>{navigation.replace(text1)}}])
                    //
                    //

                }
                else
                    Alert.alert("Message",responseJson['msg']+"")
             })
             .catch((error) => {
                //console.error(error);
                Alert.alert("Message","Try again!")
             });
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.text_header}>Welcome!</Text>
            </View>
            <View style={styles.footer}>
                <Text style={styles.text_footer}>Mobile Number</Text>
                <View style={styles.action}>
                    <FontAwesome
                        name='user-o'
                        color='#05375a'
                        size={20}/>
                    <TextInput
                        placeholder="Your Mobile No."
                        style={styles.textInput}
                        keyboardType="numeric"
                        onChangeText={(val)=>textInputChange(val)}/>
                    {data.check_textInputChange ?
                    <Feather
                        name='check-circle'
                        color='green'
                        size={20}/>
                    :null}
                </View>    
                <Text style={styles.text_footer}>Password</Text>
                <View style={styles.action}>
                    <Feather
                        name='lock'
                        color='#05375a'
                        size={20}/>
                    <TextInput
                        placeholder="Your Password"
                        autoCapitalize="none"
                        onChangeText={(val)=>handlePasswordchange(val)}
                        secureTextEntry={data.secureTextEntry?true:false}
                        style={styles.textInput}/>
                    <TouchableOpacity onPress={updateSecureTextEntry}>
                     {data.secureTextEntry?   
                    <Feather
                        name='eye-off'
                        color='grey'
                        size={20}/>
                    :
                    <Feather
                        name='eye'
                        color='grey'
                        size={20}/>
                     }
                    </TouchableOpacity>
                </View>    
                <View style={styles.buttom}>
                    <TouchableOpacity style={styles.signIn1} onPress={validate_func}>
                     <LinearGradient
                        colors={['#08d4c4','#01ab9d']}
                        style={styles.signIn}>
                        <Text style={[styles.textSign,{color:'#fff'}]}>Sign In</Text>    
                     </LinearGradient>
                     </TouchableOpacity>
                     <TouchableOpacity
                        onPress={()=>navigation.navigate("SignUp")}
                        style={[styles.signIn,{borderColor:'#009387',borderWidth:1,marginTop:15}]}>
                        <Text style={[styles.textSign,{color:'#009387'}]}>Sign Up</Text>    

                     </TouchableOpacity>
                </View>
            </View>
    
        </View>
    );
}
export default SignInScreen;


const {height}=Dimensions.get("screen");
const height_logo =height*0.28;

const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#009387'
    },
    header:{
        flex:1,
        justifyContent:"flex-end",
        paddingHorizontal:20,
        paddingBottom:30
    },
    footer:{
        flex:3,
        backgroundColor:"#fff",
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        paddingVertical:30,
        paddingHorizontal:20
    },
    text_header:{
        color:'#fff',
        fontWeight:'bold',
        fontSize:30
        
    },
    text_footer:{
        color:'#05375a',
        fontSize:18
    },
    action:{
        flexDirection:'row',
        marginTop:10,
        borderBottomStartRadius:1,
        borderBottomColor:'#f2f2f2',
        paddingBottom:5
    },
    textInput:{
        flex:1,
        marginTop:-12,
        paddingLeft:10,
        color:'#05375a'
    },
    title:{
        color:'#05375a',
        fontSize:30,
        fontWeight:'bold'
    },
    text:{
        color:'grey',
        marginTop:5
    },
    buttom:{
        alignItems:'flex-end',
        marginTop:30
    },
    signIn:{
        width:'100%',
        height:50,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:10
        },
    textSign:{
        color:'white',
        fontSize:18,
        fontWeight:"bold"
    },
    btn:{
        padding:15,
        marginTop:20,
        borderRadius:30,
        marginLeft:150,
        alignItems:'center',
       backgroundColor:'#009387'
    },
    signIn1:{
        width:'100%',
        height:50,
        borderRadius:10
     }
});