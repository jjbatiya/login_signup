import React from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from    '@react-native-community/async-storage';
import {
    View,
    Text,
    Button,
    StyleSheet,Dimensions,TextInput,TouchableOpacity, ToastAndroid, Alert
}from 'react-native';

let reg = /^[0-9]{10}$/;
const SignUpScreen=({navigation})=>
{
    const[data,setData]=React.useState({
       mobile_no:'',
       password:'',
       con_password:'',
       check_textInputChange:false,
       secureTextEntry:true ,
       secureTextEntry1:true,
    });
    const textInputChange=(val)=>
    {

        
        if (reg.test(val) != false) {
            setData({
                ...data,
                mobile_no:val,
                 check_textInputChange:true
            })
        }
        else{
            setData({
                ...data,
                mobile_no:val,
                 check_textInputChange:false
            })
        }
    }
    const handlePasswordchange=(val)=>
    {
        setData({
            ...data,
            password:val
        })
    }
    const updateSecureTextEntry=()=>{
        setData({
            ...data,
            secureTextEntry:!data.secureTextEntry
        })
    }
    const handlePasswordchange1=(val)=>
    {
        setData({
            ...data,
            con_password:val
        })
    }
    const updateSecureTextEntry1=()=>{
        setData({
            ...data,
            secureTextEntry1:!data.secureTextEntry1
        })
    }
    const validate_func=()=>
    {
        
        if(!data.check_textInputChange)
        {
            ToastAndroid.show("Mobile is Not Valid!",ToastAndroid.SHORT);
            return;
        }
        else if(data.password.length==0)
        {
            ToastAndroid.show("Password is Empty!",ToastAndroid.SHORT);
            return;
        }
        else if(data.password!=data.con_password)
        {
            ToastAndroid.show("Password and Confirm Password are must be same!",ToastAndroid.SHORT);
            return;
        }
        else
        {
            fetch('https://jitubatiya.000webhostapp.com/react_demo/api.php', {
                method: 'post',
                header:{
                  'Accept':'application/json',
                  'Content-type':'application/json'
                },
                body:JSON.stringify({
                    type:"addData",
                    name:'',
                    mobile_no:data.mobile_no,
                    u_password:data.password
                })
             })
             .then((response) => response.json())
             .then((responseJson) => {
                //var obj = JSON.parse(response);
                console.log(responseJson['message']);
                if(responseJson['Action']=='1')
                {
                    Alert.alert("Message",responseJson['message']+"",[{text:"OK",onPress:()=>{navigation.replace("SignIn")}}])
                     //AsyncStorage.setItem('isLoggedIn','1');
                }
                else
                    Alert.alert("Message",responseJson['message']+"")
          
             })
             .catch((error) => {
                Alert.alert("Message","Try again!")
             });
          
        }
    }
      
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.text_header}>Register Now!</Text>
            </View>
            <View style={styles.footer}>
                <Text style={styles.text_footer}>Mobile Number</Text>
                <View style={styles.action}>
                    <FontAwesome
                        name='user-o'
                        color='#05375a'
                        size={20}/>
                    <TextInput
                        placeholder="Your Mobile No."
                        style={styles.textInput}
                        keyboardType="numeric"
                        onChangeText={(val)=>textInputChange(val)}/>
                    {data.check_textInputChange ?
                    <Feather
                        name='check-circle'
                        color='green'
                        size={20}/>
                    :null}
                </View>    
                <Text style={styles.text_footer}>Password</Text>
                <View style={styles.action}>
                    <Feather
                        name='lock'
                        color='#05375a'
                        size={20}/>
                    <TextInput
                        placeholder="Your Password"
                        autoCapitalize="none"
                        onChangeText={(val)=>handlePasswordchange(val)}
                        secureTextEntry={data.secureTextEntry?true:false}
                        style={styles.textInput}/>
                    <TouchableOpacity onPress={updateSecureTextEntry}>
                     {data.secureTextEntry?   
                    <Feather
                        name='eye-off'
                        color='grey'
                        size={20}/>
                    :
                    <Feather
                        name='eye'
                        color='grey'
                        size={20}/>
                     }
                    </TouchableOpacity>
                </View>
                <Text style={styles.text_footer}>Confirm Password</Text>
                <View style={styles.action}>
                    <Feather
                        name='lock'
                        color='#05375a'
                        size={20}/>
                    <TextInput
                        placeholder="Your Password"
                        autoCapitalize="none"
                        onChangeText={(val)=>handlePasswordchange1(val)}
                        secureTextEntry={data.secureTextEntry1?true:false}
                        style={styles.textInput}/>
                    <TouchableOpacity onPress={updateSecureTextEntry1}>
                     {data.secureTextEntry1?   
                    <Feather
                        name='eye-off'
                        color='grey'
                        size={20}/>
                    :
                    <Feather
                        name='eye'
                        color='grey'
                        size={20}/>
                     }
                    </TouchableOpacity>
                </View>    
                <View style={styles.buttom}>
                <TouchableOpacity style={styles.signIn1} onPress={validate_func}>

                     <LinearGradient
                        onPress={validate_func}
                        colors={['#08d4c4','#01ab9d']}
                        style={styles.signIn}>
                        <Text style={[styles.textSign,{color:'#fff'}]}>Sign Up</Text>    
                    </LinearGradient>
                    </TouchableOpacity>

                     <TouchableOpacity
                        onPress={()=>navigation.goBack()}
                        style={[styles.signIn,{borderColor:'#009387',borderWidth:1,marginTop:15}]}>
                        <Text style={[styles.textSign,{color:'#009387'}]}>Sign In</Text>    

                     </TouchableOpacity>
                </View>
            </View>
    
        </View>
    );
}
export default SignUpScreen;


const {height}=Dimensions.get("screen");
const height_logo =height*0.28;

const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#009387'
    },
    header:{
        flex:1,
        justifyContent:"flex-end",
        paddingHorizontal:20,
        paddingBottom:30
    },
    footer:{
        flex:3,
        backgroundColor:"#fff",
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        paddingVertical:30,
        paddingHorizontal:20
    },
    text_header:{
        color:'#fff',
        fontWeight:'bold',
        fontSize:30
        
    },
    text_footer:{
        color:'#05375a',
        fontSize:18
    },
    action:{
        flexDirection:'row',
        marginTop:10,
        borderBottomStartRadius:1,
        borderBottomColor:'#f2f2f2',
        paddingBottom:5
    },
    textInput:{
        flex:1,
        marginTop:-12,
        paddingLeft:10,
        color:'#05375a'
    },
    title:{
        color:'#05375a',
        fontSize:30,
        fontWeight:'bold'
    },
    text:{
        color:'grey',
        marginTop:5
    },
    buttom:{
        alignItems:'flex-end',
        marginTop:30
    },
    signIn:{
        width:'100%',
        height:50,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:10
        },
    signIn1:{
        width:'100%',
        height:50,
        borderRadius:10
     },
    textSign:{
        color:'white',
        fontSize:18,
        fontWeight:"bold"
    },
    btn:{
        padding:15,
        marginTop:20,
        borderRadius:30,
        marginLeft:150,
        alignItems:'center',
       backgroundColor:'#009387'
    }
});