import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import SplashScreen from './SplashScreen';
import SignInScreen from './SignInScreen';
import SignUpScreen from './SignUpScreen';
import HomeScreen  from  './HomeScreen';
import AdminHomeScreen from './AdminHomeScreen';
import EditProduct from './EditProduct';
import AddProduct from './AddProduct';
import ProductDetail from './ProductDetail';
import UserCart from './UserCart';

const RootStack=createStackNavigator();

const RootStackScreen=({navigation})=>(
    <RootStack.Navigator headerMode=''>
        <RootStack.Screen name='Splash' component={SplashScreen}></RootStack.Screen>
        <RootStack.Screen name='SignIn' component={SignInScreen}></RootStack.Screen>
        <RootStack.Screen name='SignUp' component={SignUpScreen}></RootStack.Screen>
        <RootStack.Screen name='Home' component={HomeScreen}></RootStack.Screen>
        <RootStack.Screen name='Admin' component={AdminHomeScreen}></RootStack.Screen>
        <RootStack.Screen name='EditData' component={EditProduct}></RootStack.Screen>
        <RootStack.Screen name='AddProduct' component={AddProduct}></RootStack.Screen>
        <RootStack.Screen name='DetailPage' component={ProductDetail}></RootStack.Screen>
        <RootStack.Screen name='usercart' component={UserCart}></RootStack.Screen>

    </RootStack.Navigator>
)
export default RootStackScreen;