import AsyncStorage from '@react-native-community/async-storage';
import React from 'react';
import {
    View,
    Text,
    Button,
    StyleSheet,Dimensions,TextInput,TouchableOpacity,ToastAndroid,Alert,FlatList,Image
}from 'react-native';
var callIcon = "https://img.icons8.com/color/48/000000/phone.png";
class HomeScreen extends React.Component
{
    constructor(props) {
        super(props);

        this.state = {
            dataSource: [],
            user_id:''
        };
      }
      componentDidMount(){
        AsyncStorage.getItem("user_id").then((value) => {
            
          this.setState({
              user_id:value
          })
      });
        fetch('https://jitubatiya.000webhostapp.com/react_demo/api.php?type1=getData', {
          method: 'get',
          header:{
            'Accept':'application/json',
            'Content-type':'application/json'
          }
       })
       .then((response) => response.json())
       .then((responseJson) => {
          //var obj = JSON.parse(response);
          console.log(responseJson);
          this.setState({
            dataSource:responseJson['data']
          })
          
          //Alert.alert(""+responseJson['message'])
    
       })
       .catch((error) => {
          console.error(error);
       });  
      }
      method=()=>
      {
            AsyncStorage.clear();
            this.props.navigation.replace("SignIn");
      }
      renderItem = ({item}) => 
      {
    //    var callIcon = "https://img.icons8.com/color/48/000000/phone.png";
      //  if(item.video == true) {
          //callIcon = "https://img.icons8.com/color/48/000000/video-call.png";
        //}
        if(item.product_image==null||item.product_image=='')
          item.product_image='https://homepages.cae.wisc.edu/~ece533/images/tulips.png';
        return (
          <TouchableOpacity onPress={() =>this.props.navigation.navigate('DetailPage',item)}>
            <View style={styles.row}>
              <Image source={{ uri: item.product_image}} style={styles.pic} />
              <View>
                <View style={styles.nameContainer}>
                  <Text style={styles.nameTxt}>{item.product_name}</Text>
                </View>
                
                <View style={styles.nameContainer}>
                    <Text style={styles.nameTxt}>Rs:{item.product_price}</Text>
                </View>
                <View style={styles.nameContainer}>
                    <Text style={styles.nameTxt1}>CURRENT QTY:{item.product_qty}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        );
      }
    render(){        
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.text_header}>INVENTORY</Text>
                    <TouchableOpacity style={styles.txtRight} onPress={() =>this.props.navigation.navigate('usercart',this.state.user_id)}>
                      <Text style={styles.end}>Cart</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.txtRight} onPress={this.method}>
                      <Text style={styles.end}>Logout</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.footer}>
                    <FlatList 
                        extraData={this.state}
                        data={this.state.dataSource}
                        keyExtractor = {(item) => {return item.product_id;}}
                        renderItem={this.renderItem}/>               
                </View>
            </View>
        )
    }
}
export default HomeScreen;
const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#009387'
    },
    header:{
        flex:2,
        justifyContent:'center',
        flexDirection:'row',
        alignItems:'center',
        
    },
    txtRight:{
          justifyContent: 'flex-start',
          alignItems: 'flex-end'


    },
    footer:{
        flex:20,
        backgroundColor:"#fff",
    },
    text_header:{
        color:'#fff',
        fontWeight:'bold',
        fontSize:20,
        textAlign:"center",        
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: '#dcdcdc',
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        padding: 10,
        justifyContent: 'space-between',
    
      },
      pic: {
    
        width: 50,
        height: 50
      },
      nameContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 270,
      },
      nameTxt: {
        marginLeft: 30,
        fontWeight: '600',
        color: '#222',
        fontSize: 18,
    
      },
      nameTxt1: {
        marginLeft: 30,
        fontWeight: '600',
        color: 'red',
        fontSize: 15,
    
      },
      mblTxt: {
        fontWeight: '200',
        color: '#777',
        fontSize: 13,
      },
      end: {
        alignItems: 'flex-end',
        justifyContent:'flex-end',
        marginLeft:30,
        fontSize:20,
        color:'#fff',
        fontWeight:"bold"
      },
      icon:{
        height: 28,
        width: 28, 
      }
    
});
