import React from 'react';
//



import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
} from 'react-native';

import RootStackScreen from './screens/RootStackScreen';
import { NavigationContainer } from '@react-navigation/native';

const Main=()=>
{
    return(
        <NavigationContainer>
            <RootStackScreen></RootStackScreen>
        </NavigationContainer>
    );
}
export default Main;